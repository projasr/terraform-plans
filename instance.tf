terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

variable "filter-name" {
  type    = list(string)
  default = ["gocd-*"]
}

provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = var.filter-name
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "instance" {
  ami                  = data.aws_ami.ami.id
  instance_type        = "t2.medium"
  key_name             = "gocd-master-migration-test"
  subnet_id            = "subnet-8909c3fe"
  iam_instance_profile = "ec2GoCDServerInstanceProfile"
  vpc_security_group_ids = [
    "sg-0e6d56b4203779dad",
    "sg-30dd6155",
  ]
}

output "instance_ip" {
  value = aws_instance.instance.private_ip
}
